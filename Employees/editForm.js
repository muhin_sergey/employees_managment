﻿async function renderEditForm(id, selectedProfession) {
    if (addOrEditFormOpened) {
        return;
    }
    addOrEditFormOpened = true;

    const employee = await getEmployeeById(id);

    rednerEditFormInputs(employee, selectedProfession);
}

function rednerEditFormInputs(employee, selectedProfession) {
    selectedProfession = selectedProfession || employee.profession;

    const editForm = document.getElementById('addOrEditForm');
    const fragment = document.createDocumentFragment();

    renderHeader(fragment, 'Edit Employee');

    renderSelect(fragment, employee, selectedProfession);

    renderInputs(fragment, employee, selectedProfession);

    const divTag = document.createElement('div');
    divTag.id = 'errorMsg';

    fragment.appendChild(divTag);

    addButtonsToFragment(fragment, employee);

    editForm.appendChild(fragment);
}

function renderInputs(fragment, employee, selectedProfession) {
    const fnameInput = document.createElement('input');
    fnameInput.placeholder = 'First name';
    fnameInput.id = 'fnameInput';
    fnameInput.value = employee.fname;

    appendDivWithElement(fragment, fnameInput);

    const lnameInput = document.createElement('input');
    lnameInput.placeholder = 'Last name';
    lnameInput.id = 'lnameInput';
    lnameInput.value = employee.lname;

    appendDivWithElement(fragment, lnameInput);

    renderProfessionInput(fragment, employee, selectedProfession);

    const salaryInput = document.createElement('input');
    salaryInput.placeholder = 'Salary';
    salaryInput.id = 'salaryInput';
    salaryInput.value = employee.salary;

    appendDivWithElement(fragment, salaryInput);
}

function renderProfessionInput(fragment, employee, selectedProfession) {
    const profInput = document.createElement('input');
    profInput.placeholder = professionInputMapping[selectedProfession];
    profInput.id = 'profInput';
    if (employee.profession === selectedProfession) {
        profInput.value = employee.profession === 'QA' ? employee.type : employee.language;
    }

    appendDivWithElement(fragment, profInput);
}


function renderSelect(fragment, employee, selectedProfession) {
    const select = document.createElement('select');
    select.id = 'professionSelector';

    ['Programmer', 'QA engineer'].forEach((profession) => {
        const opt = document.createElement('option');
        opt.innerText = profession;
        opt.value = profession;
        select.appendChild(opt);
    });

    select.value = selectedProfession;

    appendDivWithElement(fragment, select);

    select.addEventListener('change',
        () => {
            disposeAddOrEditForm();
            rednerEditFormInputs(employee, select.value);
        });
}

function addButtonsToFragment(fragment, employee) {
    appendDivWithElement(fragment);
    const exitButton = document.createElement('button');
    exitButton.onclick = disposeAddOrEditForm;
    exitButton.innerText = 'Close';

    const submitButton = document.createElement('button');
    submitButton.innerText = 'Submit';
    submitButton.addEventListener('click', () => parseAndSaveEmployee(employee));

    fragment.appendChild(exitButton);
    fragment.appendChild(submitButton);
}

async function parseAndSaveEmployee(employee) {
    const select = document.getElementById('professionSelector');

    const fname = document.getElementById('fnameInput').value;
    const lname = document.getElementById('lnameInput').value;
    const salary = document.getElementById('salaryInput').value;

    const professionDetails = document.getElementById('profInput').value;

    if (!fname || !lname || !professionDetails) {
        document.getElementById('errorMsg').innerHTML = 'Fields are required';
        return;
    }

    employee = _.pick(employee, 'id');

    employee.lname = lname;
    employee.fname = fname;
    employee.salary = salary;
    employee.profession = select.value;

    switch (select.value) {
        case 'Programmer':
            employee.language = professionDetails;
            break;
        case 'QA engineer':
            employee.type = professionDetails;
            break;
    }

    await updateEmployee(employee);
    renderTable();
    disposeAddOrEditForm();
}