﻿var addOrEditFormOpened = false;

function renderAddForm(profession) {
    if (addOrEditFormOpened) {
        return;
    }
    addOrEditFormOpened = true;

    const addForm = document.getElementById('addOrEditForm');
    const fragment = document.createDocumentFragment();

    renderHeader(fragment, 'Add a new employee');

    renderAddSelect(fragment, profession);

    const select = fragment.getElementById('professionSelector');

    renderAddInputs(fragment, profession ? profession : select.value);

    const divTag = document.createElement('div');
    divTag.id = 'errorMsg';

    fragment.appendChild(divTag);

    addButtonsToAddFragment(fragment);

    addForm.appendChild(fragment);
}

function renderAddInputs(fragment, profession) {
    const fnameInput = document.createElement('input');
    fnameInput.placeholder = 'First name';
    fnameInput.id = 'fnameInput';

    appendDivWithElement(fragment, fnameInput);

    const lnameInput = document.createElement('input');
    lnameInput.placeholder = 'Last name';
    lnameInput.id = 'lnameInput';

    appendDivWithElement(fragment, lnameInput);

    renderAddProfessionInput(fragment, profession);

    const salaryInput = document.createElement('input');
    salaryInput.placeholder = 'Salary';
    salaryInput.id = 'salaryInput';

    appendDivWithElement(fragment, salaryInput);
}

function renderAddProfessionInput(fragment, profession) {
    const profInput = document.createElement('input');
    profInput.placeholder = professionInputMapping[profession];
    profInput.id = 'profInput';

    appendDivWithElement(fragment, profInput);
}

function renderAddSelect(fragment, profession) {
    const select = document.createElement('select');
    select.id = 'professionSelector';

    ['Programmer', 'QA engineer'].forEach((profession) => {
        const opt = document.createElement('option');
        opt.innerText = profession;
        opt.value = profession;
        select.appendChild(opt);
    });

    select.value = profession ? profession : select.value;

    appendDivWithElement(fragment, select);

    select.addEventListener('change',
        () => {
            disposeAddOrEditForm();
            renderAddForm(select.value);
        });
}

function addButtonsToAddFragment(fragment) {
    appendDivWithElement(fragment);
    const exitButton = document.createElement('button');
    exitButton.onclick = disposeAddOrEditForm;
    exitButton.innerText = 'Close';

    const submitButton = document.createElement('button');
    submitButton.innerText = 'Submit';
    submitButton.onclick = parseAndAddEmployee;

    fragment.appendChild(exitButton);
    fragment.appendChild(submitButton);
}

async function parseAndAddEmployee() {
    const select = document.getElementById('professionSelector');

    const fname = document.getElementById('fnameInput').value;
    const lname = document.getElementById('lnameInput').value;
    const salary = document.getElementById('salaryInput').value;

    const professionDetails = document.getElementById('profInput').value;

    if (!fname || !lname || !professionDetails) {
        document.getElementById('errorMsg').innerHTML = 'Fields are required';
        return;
    }

    let newEmployee = {};

    switch (select.value) {
        case 'Programmer':
            newEmployee = new Programmer(fname, lname, professionDetails, salary);
            break;
        case 'QA engineer':
            newEmployee = new TestEngineer(fname, lname, professionDetails, salary);
            break;
    }

    await addEmployee(newEmployee);
    renderTable();
    disposeAddOrEditForm();
}



function disposeAddOrEditForm() {
    document.getElementById('addOrEditForm').innerHTML = '';
    addOrEditFormOpened = false;
}