function renderTable() {
    disposeTable();
    disposeAddOrEditForm();
    disposeDetailsForm();

    renderLoadingText();

    var tableEmployees = [];

    getEmployees().then((result) => {

        tableEmployees = result.slice(0);

        hideLoadingText();

        $('#hideEmployeesList').css('display', 'block');

        $('#employeesListTable').DataTable({
            data: tableEmployees,
            columns: [
                { data: 'fname' },
                { data: 'lname' },
                { data: 'profession' },
                {
                    render: (data, type, row) => `<button onclick='renderDetailsForm(${row.id})'>View Details</button> <button onclick='deleteEmployee(${row.id})'>Delete</button> <button onclick='renderEditForm(${row.id})'>Edit</button>`
    }
            ],
            destroy: true
        });
    });
}

function renderLoadingText() {
    $('#loading').css('display', 'block');
}

function hideLoadingText() {
    $('#loading').css('display', 'none');
}

async function deleteEmployee(id) {
    await deleteEmployeeFromMain(id);
    renderTable();
}

function disposeTable() {
    $('#hideEmployeesList').css('display', 'none');
}