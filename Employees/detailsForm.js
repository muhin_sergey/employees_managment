﻿async function renderDetailsForm(id) {
    disposeAddOrEditForm();
    disposeTable();

    renderLoadingText();

    const employee = await getEmployeeById(id);

    hideLoadingText();

    $('#hideEmployeesList').css('display', 'none');

    $('#hideEmployeeDetails').css('display', 'block');
    
    $('#employeeDetailsTable').css('display', 'block').DataTable({
        data: [employee],
        columns: [
            { data: 'fname' },
            { data: 'lname' },
            { data: 'profession' },
            { data: 'id' },
            { data: 'salary' },
            {
                'render': (data, type, row) => {
                    var additionalInfo = _.omit(row, "fname", "lname", "profession", "id", "salary");
                    return _.reduce(Object.keys(additionalInfo),
                        (cur, key) =>
                        cur += `${key}: ${additionalInfo[key]}`,
                        "");
                }
            }
        ],
        searching: false,
        paging: false,
        ordering: false,
        destroy: true
    });
}

function disposeDetailsForm() {
    $('#hideEmployeeDetails').css('display', 'none');
}

function goBack() {
    $('#hideEmployeeDetails').css('display', 'none');
    renderTable();
}