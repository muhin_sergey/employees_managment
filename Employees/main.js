﻿function* idGenerator() {
    let id = 0;
    while (true) {
        yield id++;
    }
}

const idIterator = idGenerator();

class Employee {
    constructor(fname, lname, profession, salary) {
        this.fname = fname;
        this.lname = lname;
        this.profession = profession;
        this.id = idIterator.next().value;
        this.salary = salary;
    }
}

class Programmer extends Employee {
    constructor(fname, lname, language, salary) {
        super(fname, lname, 'Programmer', salary);
        this.language = language;
    }
}

class TestEngineer extends Employee {
    constructor(fname, lname, type, salary) {
        super(fname, lname, 'QA Engineer', salary);
        this.type = type;
    }
}


const professionInputMapping =
{
    'Programmer': 'Language',
    'QA engineer': 'Manual/Test'
}

const fieldsMapping = {
    'fname': 'First Name',
    'lname': 'Last Name',
    'profession': 'Profession',
    'language': 'Language',
    'salary': 'Salary',
    'type': 'Type',
    'id': 'Id'
}

const p1 = new Programmer('fname1', 'lname1', 'c#', 1000);
const p2 = new Programmer('fname2', 'lname2', 'js', 1200);
const t1 = new TestEngineer('fname3', 'lname3', 'manual', 1000);
const t2 = new TestEngineer('fname4', 'lname4', 'auto', 1500);

mainEmployees = [p1, p2, t1, t2];

function appendDivWithElement(fragment, element) {
    const divTag = document.createElement('div');
    if (element) {
        divTag.appendChild(element);
    }
    fragment.appendChild(divTag);
}

function renderHeader(fragment, headerText) {
    const formHeader = document.createElement('h3');
    formHeader.innerText = headerText;

    appendDivWithElement(fragment, formHeader);
}

function getEmployees() {
    return new Promise((resolve) => {
        setTimeout(() => resolve(mainEmployees), 3000);
    });
}

async function getEmployeeById(id) {
    return await new Promise((resolve) => {
        setTimeout(() => resolve(mainEmployees[id]), 1000);
    });
}

function addEmployee(employee) {
    return new Promise((resolve) => {
        setTimeout(() => {
                mainEmployees.push(employee);
                resolve();
            },
            1000);
    });
}

function updateEmployee(employee) {
    return new Promise((resolve) => {
        setTimeout(() => {
            var index = _.findIndex(mainEmployees, { id: employee.id });
            mainEmployees.splice(index, 1, employee);
            resolve();
        },
            1000);
    });
}

function deleteEmployeeFromMain(id) {
    return new Promise((resolve) => {
        setTimeout(() => {
                const ind = mainEmployees.findIndex((emp) => emp.id === id);
                mainEmployees.splice(ind, 1);
                resolve();
            },
            1000);
    });
}


var closures = function () {
    for (var i = 0; i < 10; i++) {
        (function (x) {
            setTimeout(() => console.log(x), 10);
        })(i);
    }
}